# Translation of bovo to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2009, 2016.
msgid ""
msgstr ""
"Project-Id-Version: bovo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-27 00:55+0000\n"
"PO-Revision-Date: 2016-01-06 18:29+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#. i18n: ectx: label, entry (theme), group (bovo)
#: gui/bovo.kcfg:9 gui/mainwindow.cc:204
#, kde-format
msgid "Theme"
msgstr "Tema"

#. i18n: ectx: label, entry (playbackSpeed), group (bovo)
#: gui/bovo.kcfg:13
#, kde-format
msgid "Speed of demo and replay playback."
msgstr "Farten til demoen og vising av opptak."

#. i18n: ectx: label, entry (animation), group (bovo)
#: gui/bovo.kcfg:19
#, kde-format
msgid "Whether moves should be animated or not."
msgstr "Om trekka skal animerast."

#. i18n: ectx: label, entry (ai), group (bovo)
#: gui/bovo.kcfg:23
#, kde-format
msgid "AI engine to use."
msgstr "Datamotstandaren som skal brukast."

#: gui/main.cc:52
#, kde-format
msgid "Bovo"
msgstr "Bovo"

#: gui/main.cc:53
#, kde-format
msgid "KDE Five in a Row Board Game"
msgstr "Brettspelet fem på rad"

#: gui/main.cc:54
#, kde-format
msgid "(c) 2002-2007, Aron Boström"
msgstr "© 2002–2007 Aron Boström"

#: gui/main.cc:57
#, kde-format
msgid "Aron Boström"
msgstr "Aron Boström"

#: gui/main.cc:57
#, kde-format
msgid "Author"
msgstr "Opphavsperson"

#: gui/mainwindow.cc:67 gui/mainwindow.cc:364
#, kde-format
msgid "Wins: %1"
msgstr "Sigrar: %1"

#: gui/mainwindow.cc:68 gui/mainwindow.cc:258 gui/mainwindow.cc:377
#, kde-format
msgid "Losses: %1"
msgstr "Tap: %1"

#: gui/mainwindow.cc:190
#, kde-format
msgid "&Replay"
msgstr "&Vis omgang på nytt"

#: gui/mainwindow.cc:192
#, kde-format
msgid "Replay game"
msgstr "Vis omgang på nytt"

#: gui/mainwindow.cc:193
#, kde-format
msgid "Replays your last game for you to watch."
msgstr "Vis den siste omgangen du spelte."

#: gui/mainwindow.cc:199
#, kde-format
msgid "&Animation"
msgstr "&Animasjon"

#: gui/mainwindow.cc:344
#, kde-format
msgid "Start a new Game to play"
msgstr "Start eit nytt spel"

#: gui/mainwindow.cc:382
#, kde-format
msgid "GAME OVER. Tie!"
msgstr "SPELET ER SLUTT. Uavgjort!"

#: gui/mainwindow.cc:385
#, kde-format
msgid "GAME OVER. You won!"
msgstr "Spelet er slutt. Du vann!"

#: gui/mainwindow.cc:388
#, kde-format
msgid "GAME OVER. You lost!"
msgstr "Spelet er slutt. Du tapte!"

#: gui/mainwindow.cc:400
#, kde-format
msgid "It is your turn."
msgstr "Det er din tur."

#: gui/mainwindow.cc:404
#, kde-format
msgid "Waiting for computer."
msgstr "Ventar på datamaskina."

#: gui/mainwindow.cc:432
#, kde-format
msgid "Replaying game"
msgstr "Viser omgang på nytt"

#: gui/mainwindow.cc:448
#, kde-format
msgid "Game replayed."
msgstr "Omgang vist på nytt."
